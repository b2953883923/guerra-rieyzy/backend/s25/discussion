
// [SECTION] Objects

/*
	- An object is a data type that is used to represent real world objects.
	- It is a collection of related data and/or functionalities.
	- The information stored in objects are represented in a "key: value" pair
		-"key" - referred to as the property of an object.
		-"value" - stores a specific information about the object. It can be any data types

*/

	// Object literals

		/*
			- This creates/declares an object and also initializes/assigns value to its properties upon creation

			Syntax:
				let objectName = {
					keyA: valueA,
					keyB: valueB
				}
		*/

	let cellphone = {
		name: "Nokia 3210",
		manufactureDate: 1999
	}

	console.log("The result from creating objects using literal notation: ");
	console.log(cellphone);
	console.log(typeof cellphone);

	// Contructor function

	/*
		- Creates a reusable function to create several objects that have the same data structures.
		- This is useful for creating multiple instances/copies of an object.
		- Uses PascalCasing for the FunctionName
		Syntax:

			function ObjectName(keyA, keyB){
				this.keyA = keyA;
				this.keyB = keyB;
			};

	*/


	// "this" keyword allows us to assign a new objects properties by associating them with values received from the constructon functions parameters.
	function Laptop(name, manufactureDate){
		this.name = name;
		this.manufactureDate = manufactureDate;
	}
	// The "new" keyword creates an instance of object.
	let laptop = new Laptop("Lenovo", 2008);
	console.log("Results from creating objects using constructor function:");
	console.log(laptop)

	let myLaptop = new Laptop("Mcbook Air", 2021);
	console.log("Results from creating objects using constructor function: ");
	console.log(myLaptop)

	let oldLaptop = Laptop("Portal R2E", 1980); // this will results to undefined since there is no "new" keyword;
	console.log("Results from creating objects using constructor function: ");
	console.log(oldLaptop)

	// Creating an empty object
	let computer = {};
	let myComputer = new Object();

	console.log(computer);
	console.log(myComputer);

// [SECTION] Accessing Object Properties

	//Using the dot notation
	console.log("Result from dot notation: " + myLaptop.name);

	// Using the square bracket notation
	console.log("Result from square bracket notation: " + myLaptop['name']);

// Accessing array objects

	// This array contains objects as elements.
				// 0       1
	let array = [laptop, myLaptop];
	// this approach may be confusing for accessing array indixes and object properties
	console.log(array[0]['name']);

	// This approach is much preferred than the previous one.
	console.log(array[0].name)

// [SECTION] Initializing, Adding, Deleting and Reassigning Object Properties

let car = {};

// Adding object properties using dot nation.

car.name = "Honda Civic";
console.log("Result from adding properties using dot nation")
console.log(car);

// Adding object properties using bracket notation.

car['manufacturedate'] = 2019;
console.log(car['manufacturedate']);
console.log(car['manufacture date']); // this property name is not recommended
console.log(car['manufacture Date']); // result to undefined;
console.log(car.manufactureDate); // result undefined
console.log(car);

// [SECTION] Object Method

	// a method that serves/acts like a function as part of the objects properties.

	let person = {
		john: "John",
		talk: function(){
			console.log("Hello! My name is" + this.name);
		}
	};

	console.log(person);
	console.log("Result from object methods:");
	console.log(person.name);
	person.talk();

	// Adding method to objects using dot notation.
	person.walk = function(){
		console.log(this.name+ " walked 25 steps forward.")
	}

	person.walk();

	let friend = {
		firstName: "Joe",
		lastName: "Smith",
		address: {
			city: "Austin",
			state: "Texas",
			country: "USA"
		},
		email: ["joe@mail.com", "joesmith@email.com"],
		introduce: function() {
			console.log("Hello, my name is" + this.firstName+ ' ' + this.lastName);
		}
	}

	friend.introduce();

// [SECTION] Real World Application of Objects.

	let myPokemon = {
		name: "Pikachu",
		level: 3,
		health: 100,
		attack: 50,
		tackle: function(){
			console.log("This pokemon tackled the targetPokemon");
			console.log("TargetPokemon's health is now reduced to _tagetPokemonHealth_");

		},
		faint: function(){
			console.log("This Pokemon's health has dropped to 0.");
			console.log("This Pokemon fainted");
		}
	}

	console.log(myPokemon);

	// Using Constructor Function (object contructon)
		// this will allow to create multiple objects (pokemons)

	function Pokemon(name, level){

		// Properties
		this.name = name;
		this.level = level;
		this.health = 2 * level;
		this.attack = level;

		// Method
		this.tackle = function(target){
			console.log(this.name + ' tackled ' + target.name);
			console.log("targetPokemon's health is now reduced to _targetPokemonHealth_");
		};
		this.faint = function(){
			console.log(this.name + ' faint ');
		}

	}

let pikachu = new Pokemon("Pikachu", 16);
let rattata = new Pokemon("Rattata", 8);

pikachu.tackle(rattata);